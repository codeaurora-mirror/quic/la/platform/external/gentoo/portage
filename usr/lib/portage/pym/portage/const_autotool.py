# Copyright: 2005-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

# all vars that are to wind up in portage_const must have their name listed in __all__

__all__ = ["EPREFIX", "SYSCONFDIR", "PORTAGE_BASE",
		"portageuser", "portagegroup", "rootuser", "rootuid", "rootgid",
		"PORTAGE_BASH", "PORTAGE_MV"]

import getpass
import grp
import os

EPREFIX      = os.path.realpath(os.path.abspath(__file__) + '../' * 7)
SYSCONFDIR   = os.path.join(EPREFIX, 'etc')
PORTAGE_BASE = os.path.join(EPREFIX, 'usr/lib/portage')

portagegroup = grp.getgrgid(os.getgid()).gr_name
portageuser  = getpass.getuser()
rootuser     = portageuser
rootuid      = os.getuid()
rootgid      = os.getgid()

PORTAGE_BASH = "/bin/bash"
PORTAGE_MV   = "/bin/mv"
